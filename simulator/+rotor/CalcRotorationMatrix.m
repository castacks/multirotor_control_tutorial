%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 
% Copyright (c) 2020 Carnegie Mellon University
% This tool has been developed for educational purposes only as a 
% control tutorial in Air Lab Summer School 2020 (https://theairlab.org). 
% For License information please see the LICENSE file in the root directory.
% Author: Azarakhsh Keipour (keipour [at] cmu.edu)
% Please contact us for any questions or issues.
%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 

function R = CalcRotorationMatrix(rot)
    if isempty(rot.ArmAngle)
        R = eye(3);
        return;
    end

    rotorZB = rotz(rot.ArmAngle) * rotz(90);

    rotorXp = rotx(rot.InwardAngle);

    rotorYpp = roty(rot.SidewardAngle);

    R = (rotorZB * rotorXp * rotorYpp)';
end
